# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='buy_date',
            field=models.DateField(verbose_name='date d\u2019achat'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loan',
            name='loan_date',
            field=models.DateField(verbose_name='date de pr\xeat'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loan',
            name='loan_date_end',
            field=models.DateField(null=True, verbose_name='date de fin de pr\xeat', blank=True),
            preserve_default=True,
        ),
    ]
