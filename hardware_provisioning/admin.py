# -*- coding: utf-8 -*-

from __future__ import unicode_literals


from django.contrib import admin
from django.contrib.auth import get_user_model
from django.forms import ModelChoiceField
from django.utils import timezone
import autocomplete_light

from .models import ItemType, Item, Loan, Storage
import coin.members.admin


User = get_user_model()

admin.site.register(ItemType)


class OwnerFilter(admin.SimpleListFilter):
    title = "Propriétaire"
    parameter_name = 'owner'

    def lookups(self, request, model_admin):
        owners = [
            (i.pk, i) for i in User.objects.filter(items__isnull=False)]

        return [(None, "L'association")] + owners

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(owner__pk=self.value())
        else:
            return queryset


class AvailabilityFilter(admin.SimpleListFilter):
    title = "Disponibilité"
    parameter_name = 'availability'

    def lookups(self, request, model_admin):
        return [
            ('available', 'Disponible'),
            ('borrowed', 'Emprunté'),
            ('deployed', 'Déployé'),
        ]

    def queryset(self, request, queryset):
        if self.value() == 'available':
            return queryset.available()
        elif self.value() == 'borrowed':
            return queryset.borrowed()
        elif self.value() == 'deployed':
            return queryset.deployed()
        else:
            return queryset


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = (
        'designation', 'type', 'mac_address', 'serial', 'owner',
        'buy_date', 'deployed', 'is_available')
    list_filter = (
        AvailabilityFilter, 'type__name', 'storage',
        'buy_date', OwnerFilter)
    search_fields = (
        'designation', 'mac_address', 'serial',
        'owner__email', 'owner__nickname',
        'owner__first_name', 'owner__last_name')
    save_as = True
    actions = ['give_back']

    form = autocomplete_light.modelform_factory(Loan, fields='__all__')

    def give_back(self, request, queryset):
        for item in queryset.filter(loans__loan_date_end=None):
            item.give_back()
    give_back.short_description = 'Rendre le matériel'


class StatusFilter(admin.SimpleListFilter):
    title = 'Statut'
    parameter_name = 'status'

    def lookups(self, request, model_admin):
        return [
            ('all', 'Tout'),
            (None, 'En cours'),
            ('finished', 'Passés'),
        ]

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        v = self.value()
        if v in (None, 'running'):
            return queryset.running()
        elif v == 'finished':
            return queryset.finished()
        else:
            return queryset


class BorrowerFilter(admin.SimpleListFilter):
    title = 'Adhérent emprunteur'
    parameter_name = 'user'

    def lookups(self, request, model_admin):
        users = set()
        for loan in model_admin.get_queryset(request):
            users.add((loan.user.pk, loan.user))
        return users

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(user=self.value())
        else:
            return queryset


class ItemChoiceField(ModelChoiceField):
    # On surcharge cette méthode pour afficher mac et n° de série dans le menu
    # déroulant de sélection d'un objet dans la création d'un prêt.
    def label_from_instance(self, obj):
        return obj.designation + ' ' + obj.get_mac_and_serial()

@admin.register(Loan)
class LoanAdmin(admin.ModelAdmin):
    list_display = ('item', 'get_mac_and_serial', 'user', 'loan_date', 'loan_date_end')
    list_filter = (StatusFilter, BorrowerFilter, 'item__designation')
    search_fields = (
        'item__designation',
        'user__nickname', 'user__username',
        'user__first_name', 'user__last_name', )
    actions = ['end_loan']

    def end_loan(self, request, queryset):
        queryset.filter(loan_date_end=None).update(
            loan_date_end=datetime.now())
    end_loan.short_description = 'Mettre fin au prêt'

    form = autocomplete_light.modelform_factory(Loan, fields='__all__')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'item':
            kwargs['queryset'] = Item.objects.all()
            return ItemChoiceField(**kwargs)
        else:
            return super(LoanAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Storage)
class StorageAdmin(admin.ModelAdmin):
    list_display = ('name', 'truncated_notes', 'items_count')

    def truncated_notes(self, obj):
        if len(obj.notes) > 50:
            return '{}…'.format(obj.notes[:50])
        else:
            return obj.notes
    truncated_notes.short_description = 'notes'

class LoanInline(admin.TabularInline):
    model = Loan
    extra = 0
    exclude = ('notes',)
    readonly_fields = ('item', 'get_mac_and_serial', 'loan_date', 'loan_date_end', 'is_running')

    show_change_link = True

    def get_queryset(self, request):
        qs = super(LoanInline, self).get_queryset(request)
        return qs.order_by('-loan_date_end')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class MemberAdmin(coin.members.admin.MemberAdmin):
    inlines = coin.members.admin.MemberAdmin.inlines + [LoanInline]

admin.site.unregister(coin.members.admin.Member)
admin.site.register(coin.members.admin.Member, MemberAdmin)
