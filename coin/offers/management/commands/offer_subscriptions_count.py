# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from optparse import make_option
import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q, Count

from coin.offers.models import Offer, OfferSubscription


BOLD_START = '\033[1m'
BOLD_END = '\033[0m'

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--date', action='store', dest='date',
                default=datetime.date.today(), help='Specifies the date to use. Format is YYYY-MM-DD. Default is "today".'),
    )
    help = "Return subscription count for each offer type"

    def handle(self, *args, **options):
        # Get date option
        date = options.get('date')

        # Validate date type
        if type(date) is not datetime.date:
            try:
                datetime.datetime.strptime(date, '%Y-%m-%d')
            except ValueError, TypeError:
                raise CommandError("Incorrect date format, should be YYYY-MM-DD")

        # Count offer subscription
        offers = Offer.objects\
                      .filter(Q(offersubscription__subscription_date__lte=date) & (Q(offersubscription__resign_date__gt=date) | Q(offersubscription__resign_date__isnull=True)))\
                      .annotate(num_subscribtions=Count('offersubscription'))\
                      .order_by('name')

        # Print count by offer type
        for offer in offers:
            self.stdout.write("{offer} offer has {count} subscriber(s)".format(
                offer=BOLD_START + offer.name + BOLD_END,
                count=BOLD_START + str(offer.num_subscribtions) + BOLD_END))