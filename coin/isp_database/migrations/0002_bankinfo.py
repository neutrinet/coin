# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import localflavor.generic.models


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BankInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('iban', localflavor.generic.models.IBANField(max_length=34)),
                ('bic', localflavor.generic.models.BICField(max_length=11, null=True, verbose_name='BIC', blank=True)),
                ('bank_name', models.CharField(max_length=100, null=True, verbose_name='\xc9tablissement bancaire', blank=True)),
                ('isp', models.OneToOneField(to='isp_database.ISPInfo')),
            ],
            options={
                'verbose_name': 'Coordonn\xe9es bancaires',
            },
            bases=(models.Model,),
        ),
    ]
