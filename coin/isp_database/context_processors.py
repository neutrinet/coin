# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from coin.isp_database.models import ISPInfo

def branding(request):
    """ Just a shortcut to get the ISP object in templates
    """
    return {'branding': ISPInfo.objects.first()}
