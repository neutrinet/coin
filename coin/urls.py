# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import apps
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from coin import views
import coin.apps

import autocomplete_light
autocomplete_light.autodiscover()

from django.contrib import admin
admin.autodiscover()

from coin.isp_database.views import isp_json


def apps_urlpatterns():
    """ Yields url lists ready to be appended to urlpatterns list
    """
    for app_config in apps.get_app_configs():
        if isinstance(app_config, coin.apps.AppURLs):
            for prefix, pats in app_config.exported_urlpatterns:
                yield url(
                    r'^{}/'.format(prefix),
                    include(pats, namespace=prefix))

urlpatterns = patterns(
    '',
    url(r'^$', 'coin.members.views.index', name='home'),

    url(r'^isp.json$', isp_json),
    url(r'^members/', include('coin.members.urls', namespace='members')),
    url(r'^billing/', include('coin.billing.urls', namespace='billing')),
    url(r'^subscription/', include('coin.offers.urls', namespace='subscription')),

    url(r'^admin/', include(admin.site.urls)),

    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^feed/(?P<feed_name>.+)', views.feed, name='feed'),

    url(r'^autocomplete/', include('autocomplete_light.urls')),

)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Pluggable apps URLs
urlpatterns += list(apps_urlpatterns())
