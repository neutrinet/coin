# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import logging
import uuid
import re
from decimal import Decimal
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.db import models, transaction
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse

from coin.offers.models import OfferSubscription
from coin.members.models import Member
from coin.html2pdf import render_as_pdf
from coin.utils import private_files_storage, start_of_month, end_of_month, \
                       postgresql_regexp, send_templated_email,             \
                       disable_for_loaddata
from coin.isp_database.context_processors import branding
from coin.isp_database.models import ISPInfo

accounting_log = logging.getLogger("coin.billing")


def invoice_pdf_filename(instance, filename):
    """Nom et chemin du fichier pdf à stocker pour les factures"""
    member_id = instance.member.id if instance.member else 0
    return 'invoices/%d_%s_%s.pdf' % (member_id,
                                      instance.number,
                                      uuid.uuid4())


@python_2_unicode_compatible
class InvoiceNumber:
    """ Logic and validation of invoice numbers

    Defines invoice numbers serie in a way that is legal in france.

    https://www.service-public.fr/professionnels-entreprises/vosdroits/F23208#fiche-item-3

    Our format is YYYY-MM-XXXXXX
    - YYYY the year of the bill
    - MM month of the bill
    - XXXXXX a per-month sequence
    """
    RE_INVOICE_NUMBER = re.compile(
        r'(?P<year>\d{4})-(?P<month>\d{2})-(?P<index>\d{6})')

    def __init__(self, date, index):
        self.date = date
        self.index = index

    def get_next(self):
        return InvoiceNumber(self.date, self.index + 1)

    def __str__(self):
        return '{:%Y-%m}-{:0>6}'.format(self.date, self.index)

    @classmethod
    def parse(cls, string):
        m = cls.RE_INVOICE_NUMBER.match(string)
        if not m:
            raise ValueError('Not a valid invoice number: "{}"'.format(string))

        return cls(
            datetime.date(
                year=int(m.group('year')),
                month=int(m.group('month')),
                day=1),
            int(m.group('index')))

    @staticmethod
    def time_sequence_filter(date, field_name='date'):
        """ Build queryset filter to be used to get the invoices from the
        numbering sequence of a given date.

        :param field_name: the invoice field name to filter on.

        :type date: datetime
        :rtype: dict
        """

        return {
            '{}__month'.format(field_name): date.month,
            '{}__year'.format(field_name): date.year
        }


class InvoiceQuerySet(models.QuerySet):
    def get_next_invoice_number(self, date):
        last_invoice_number_str = self._get_last_invoice_number(date)

        if last_invoice_number_str is None:
            # It's the first bill of the month
            invoice_number = InvoiceNumber(date, 1)
        else:
            invoice_number = InvoiceNumber.parse(last_invoice_number_str).get_next()

        return str(invoice_number)

    def _get_last_invoice_number(self, date):
        same_seq_filter = InvoiceNumber.time_sequence_filter(date)
        return self.filter(**same_seq_filter).with_valid_number().aggregate(
            models.Max('number'))['number__max']

    def with_valid_number(self):
        """ Excludes previous numbering schemes or draft invoices
        """
        return self.filter(number__regex=postgresql_regexp(
            InvoiceNumber.RE_INVOICE_NUMBER))


class Invoice(models.Model):

    INVOICES_STATUS_CHOICES = (
        ('open', 'À payer'),
        ('closed', 'Réglée'),
        ('trouble', 'Litige')
    )

    validated = models.BooleanField(default=False, verbose_name='validée',
                                    help_text='Once validated, a PDF is generated'
                                    ' and the invoice cannot be modified')
    number = models.CharField(max_length=25,
                              unique=True,
                              verbose_name='numéro')
    status = models.CharField(max_length=50, choices=INVOICES_STATUS_CHOICES,
                              default='open',
                              verbose_name='statut')
    date = models.DateField(
        default=datetime.date.today, null=True, verbose_name='date',
        help_text='Cette date sera définie à la date de validation dans la facture finale')
    date_due = models.DateField(
        null=True, blank=True,
        verbose_name="date d'échéance de paiement",
        help_text='Le délai de paiement sera fixé à {} jours à la validation si laissé vide'.format(settings.PAYMENT_DELAY))
    member = models.ForeignKey(Member, null=True, blank=True, default=None,
                               related_name='invoices',
                               verbose_name='membre',
                               on_delete=models.SET_NULL)
    pdf = models.FileField(storage=private_files_storage,
                           upload_to=invoice_pdf_filename,
                           null=True, blank=True,
                           verbose_name='PDF')

    date_last_reminder_email = models.DateTimeField(null=True, blank=True,
                        verbose_name="Date du dernier email de relance envoyé")

    def save(self, *args, **kwargs):
        # First save to get a PK
        super(Invoice, self).save(*args, **kwargs)
        # Then use that pk to build draft invoice number
        if not self.validated and self.pk and not self.number:
            self.number = 'DRAFT-{}'.format(self.pk)
            self.save()

    def amount(self):
        """
        Calcul le montant de la facture
        en fonction des éléments de détails
        """
        total = Decimal('0.0')
        for detail in self.details.all():
            total += detail.total()
        return total.quantize(Decimal('0.01'))
    amount.short_description = 'Montant'

    def amount_before_tax(self):
        total = Decimal('0.0')
        for detail in self.details.all():
            total += detail.amount
        return total.quantize(Decimal('0.01'))
    amount_before_tax.short_description = 'Montant HT'

    def amount_paid(self):
        """
        Calcul le montant déjà payé à partir des allocations de paiements
        """
        return sum([a.amount for a in self.allocations.all()])
    amount_paid.short_description = 'Montant payé'

    def amount_remaining_to_pay(self):
        """
        Calcul le montant restant à payer
        """
        return self.amount() - self.amount_paid()
    amount_remaining_to_pay.short_description = 'Reste à payer'

    def has_owner(self, username):
        """
        Check if passed username (ex gmajax) is owner of the invoice
        """
        return (self.member and self.member.username == username)

    def generate_pdf(self):
        """
        Make and store a pdf file for the invoice
        """
        context = {"invoice": self}
        context.update(branding(None))
        pdf_file = render_as_pdf('billing/invoice_pdf.html', context)
        self.pdf.save('%s.pdf' % self.number, pdf_file)

    @transaction.atomic
    def validate(self):
        """
        Switch invoice to validate mode. This set to False the draft field
        and generate the pdf
        """
        self.date = datetime.date.today()
        if not self.date_due:
            self.date_due = self.date + datetime.timedelta(days=settings.PAYMENT_DELAY)
        old_number = self.number
        self.number = Invoice.objects.get_next_invoice_number(self.date)

        self.validated = True
        self.save()
        self.generate_pdf()

        accounting_log.info(
            "Draft invoice {} validated as invoice {}. ".format(
                old_number, self.number) +
            "(Total amount : {} ; Member : {})".format(
                self.amount(), self.member))
        assert self.pdf_exists()
        if self.member is not None:
            update_accounting_for_member(self.member)


    def pdf_exists(self):
        return (self.validated
                and bool(self.pdf)
                and private_files_storage.exists(self.pdf.name))

    def get_absolute_url(self):
        return reverse('billing:invoice', args=[self.number])

    def __unicode__(self):
        return '#{} {:0.2f}€ {}'.format(
            self.number, self.amount(), self.date_due)

    def reminder_needed(self):

        # If there's no member, there's nobody to be reminded
        if self.member is None:
            return False

        # If bill is close or not validated yet, nope
        if self.status != 'open' or not self.validated:
            return False

        # If bill is not at least one month old, nope
        if self.date_due >= timezone.now()+relativedelta(weeks=-4):
            return False

        # If a reminder has been recently sent, nope
        if (self.date_last_reminder_email
            and (self.date_last_reminder_email
                 >= timezone.now() + relativedelta(weeks=-3))):
            return False

        return True

    def send_reminder(self, auto=False):
        """ Envoie un courrier pour rappeler à un abonné qu'une facture est
        en attente de paiement

        :param bill: id of the bill to remind
        :param auto: is it an auto email? (changes slightly template content)
        """

        if not self.reminder_needed():
            return False

        accounting_log.info(
            "Sending reminder email to {} to pay invoice {}".format(
                self.member, str(self.number)))

        isp_info = ISPInfo.objects.first()
        kwargs = {}
        # Il peut ne pas y avir d'ISPInfo, ou bien pas d'administrative_email
        if isp_info and isp_info.administrative_email:
            kwargs['from_email'] = isp_info.administrative_email

        # Si le dernier courriel de relance a été envoyé il y a moins de trois
        # semaines, n'envoi pas un nouveau courriel
        send_templated_email(
            to=self.member.email,
            subject_template='billing/emails/reminder_for_unpaid_bill.txt',
            body_template='billing/emails/reminder_for_unpaid_bill.html',
            context={'member': self.member, 'branding': isp_info,
                     'membership_info_url': settings.MEMBER_MEMBERSHIP_INFO_URL,
                     'today': datetime.date.today,
                     'auto_sent': auto},
            **kwargs)

        # Sauvegarde en base la date du dernier envoi de mail de relance
        self.date_last_reminder_email = timezone.now()
        self.save()
        return True

    class Meta:
        verbose_name = 'facture'

    objects = InvoiceQuerySet().as_manager()


class InvoiceDetail(models.Model):

    label = models.CharField(max_length=100)
    amount = models.DecimalField(max_digits=5, decimal_places=2,
                                 verbose_name='montant')
    quantity = models.DecimalField(null=True, verbose_name='quantité',
                                   default=1.0, decimal_places=2, max_digits=4)
    tax = models.DecimalField(null=True, default=0.0, decimal_places=2,
                              max_digits=4, verbose_name='TVA',
                              help_text='en %')
    invoice = models.ForeignKey(Invoice, verbose_name='facture',
                                related_name='details')
    offersubscription = models.ForeignKey(OfferSubscription, null=True,
                                          blank=True, default=None,
                                          verbose_name='abonnement')
    period_from = models.DateField(
        default=start_of_month,
        null=True,
        blank=True,
        verbose_name='début de période',
        help_text='Date de début de période sur laquelle est facturé cet item')
    period_to = models.DateField(
        default=end_of_month,
        null=True,
        blank=True,
        verbose_name='fin de période',
        help_text='Date de fin de période sur laquelle est facturé cet item')

    def __unicode__(self):
        return self.label

    def total(self):
        """Calcul le total"""
        return (self.amount * (self.tax / Decimal('100.0') +
                               Decimal('1.0')) *
                self.quantity).quantize(Decimal('0.01'))

    class Meta:
        verbose_name = 'détail de facture'


class Payment(models.Model):

    PAYMENT_MEAN_CHOICES = (
        ('cash', 'Espèces'),
        ('check', 'Chèque'),
        ('transfer', 'Virement'),
        ('other', 'Autre')
    )

    member = models.ForeignKey(Member, null=True, blank=True, default=None,
                               related_name='payments',
                               verbose_name='membre',
                               on_delete=models.SET_NULL)

    payment_mean = models.CharField(max_length=100, null=True,
                                    default='transfer',
                                    choices=PAYMENT_MEAN_CHOICES,
                                    verbose_name='moyen de paiement')
    amount = models.DecimalField(max_digits=5, decimal_places=2, null=True,
                                 verbose_name='montant')
    date = models.DateField(default=datetime.date.today)
    invoice = models.ForeignKey(Invoice, verbose_name='facture associée', null=True,
                                blank=True, related_name='payments')

    label = models.CharField(max_length=500,
                             null=True, blank=True, default="",
                             verbose_name='libellé')

    def save(self, *args, **kwargs):

        # Only if no amount already allocated...
        if self.amount_already_allocated() == 0:

            # If there's a linked invoice and no member defined
            if self.invoice and not self.member:
                # Automatically set member to invoice's member
                self.member = self.invoice.member

        super(Payment, self).save(*args, **kwargs)


    def clean(self):

        # Only if no amount already alloca ted...
        if self.amount_already_allocated() == 0:

            # If there's a linked invoice and this payment would pay more than
            # the remaining amount needed to pay the invoice...
            if self.invoice and self.amount > self.invoice.amount_remaining_to_pay():
                raise ValidationError("This payment would pay more than the invoice's remaining to pay")

    def amount_already_allocated(self):
        return sum([ a.amount for a in self.allocations.all() ])

    def amount_not_allocated(self):
        return self.amount - self.amount_already_allocated()

    @transaction.atomic
    def allocate_to_invoice(self, invoice):

        # FIXME - Add asserts about remaining amount > 0, unpaid amount > 0,
        # ...

        amount_can_pay = self.amount_not_allocated()
        amount_to_pay  = invoice.amount_remaining_to_pay()
        amount_to_allocate = min(amount_can_pay, amount_to_pay)

        accounting_log.info(
            "Allocating {} from payment {} to invoice {}".format(
                amount_to_allocate, self.date, invoice.number))

        PaymentAllocation.objects.create(invoice=invoice,
                                         payment=self,
                                         amount=amount_to_allocate)

        # Close invoice if relevant
        if (invoice.amount_remaining_to_pay() <= 0) and (invoice.status == "open"):
            accounting_log.info(
                "Invoice {} has been paid and is now closed".format(
                    invoice.number))
            invoice.status = "closed"

        invoice.save()
        self.save()

    def __unicode__(self):
        if self.member is not None:
            return 'Paiment de {:0.2f}€ le {} par {}'.format(
                self.amount, self.date, self.member)
        else:
            return 'Paiment de {:0.2f}€ le {}'.format(
                self.amount, self.date)

    class Meta:
        verbose_name = 'paiement'


# This corresponds to a (possibly partial) allocation of a given payment to
# a given invoice.
# E.g. consider an invoice I with total 15€ and a payment P with 10€.
# There can be for example an allocation of 3.14€ from P to I.
class PaymentAllocation(models.Model):

    invoice = models.ForeignKey(Invoice, verbose_name='facture associée',
                                null=False, blank=False,
                                related_name='allocations')
    payment = models.ForeignKey(Payment, verbose_name='facture associée',
                                null=False, blank=False,
                                related_name='allocations')
    amount = models.DecimalField(max_digits=5, decimal_places=2, null=True,
                                 verbose_name='montant')


def get_active_payment_and_invoices(member):

    # Fetch relevant and active payments / invoices
    # and sort then by chronological order : olders first, newers last.

    this_member_invoices = [i for i in member.invoices.filter(validated=True).order_by("date")]
    this_member_payments = [p for p in member.payments.order_by("date")]

    # TODO / FIXME ^^^ maybe also consider only 'opened' invoices (i.e. not
    # conflict / trouble invoices)

    active_payments = [p for p in this_member_payments if p.amount_not_allocated()    > 0]
    active_invoices = [p for p in this_member_invoices if p.amount_remaining_to_pay() > 0]

    return active_payments, active_invoices


def update_accounting_for_member(member):
    """
    Met à jour le status des factures, des paiements et le solde du compte
    d'un utilisateur
    """

    accounting_log.info("Updating accounting for member {} ...".format(member))
    accounting_log.info(
        "Member {} current balance is {} ...".format(member, member.balance))

    reconcile_invoices_and_payments(member)

    this_member_invoices = [i for i in member.invoices.filter(validated=True).order_by("date")]
    this_member_payments = [p for p in member.payments.order_by("date")]

    member.balance = compute_balance(this_member_invoices,
                                     this_member_payments)
    member.save()

    accounting_log.info("Member {} new balance is {:f}".format(
        member, member.balance))


def reconcile_invoices_and_payments(member):
    """
    Rapproche des factures et des paiements qui sont actifs (paiement non alloué
    ou factures non entièrement payées) automatiquement.
    """

    active_payments, active_invoices = get_active_payment_and_invoices(member)

    if active_payments == []:
        accounting_log.info(
            "(No active payment for {}.".format(member)
            + " No invoice/payment reconciliation needed.).")
        return
    elif active_invoices == []:
        accounting_log.info(
            "(No active invoice for {}. No invoice/payment ".format(member) +
            "reconciliation needed.).")
        return

    accounting_log.info(
        "Initiating reconciliation between invoice and payments for {}".format(
            member))

    while active_payments != [] and active_invoices != []:

        # Only consider the oldest active payment and the oldest active invoice
        p = active_payments[0]

        # If this payment is to be allocated for a specific invoice...
        if p.invoice:
            # Assert that the invoice is still 'active'
            assert p.invoice in active_invoices
            i = p.invoice
            accounting_log.info(
                "Payment is to be allocated specifically to invoice {}".format(
                    i.number))
        else:
            i = active_invoices[0]

        # TODO : should add an assert that the ammount not allocated / remaining to
        # pay is lower before and after calling the allocate_to_invoice

        p.allocate_to_invoice(i)

        active_payments, active_invoices = get_active_payment_and_invoices(member)

    if active_payments == []:
        accounting_log.info("No more active payment. Nothing to reconcile anymore.")
    elif active_invoices == []:
        accounting_log.info("No more active invoice. Nothing to reconcile anymore.")
    return


def compute_balance(invoices, payments):

    active_payments = [p for p in payments if p.amount_not_allocated()    > 0]
    active_invoices = [i for i in invoices if i.amount_remaining_to_pay() > 0]

    s = 0
    s -= sum([i.amount_remaining_to_pay() for i in active_invoices])
    s += sum([p.amount_not_allocated()    for p in active_payments])

    return s


@receiver(post_save, sender=Payment)
@disable_for_loaddata
def payment_changed(sender, instance, created, **kwargs):

    if created:
        accounting_log.info("Adding payment %s (Date: %s, Member: %s, Amount: %s, Label: %s)."
                            % (instance.pk, instance.date, instance.member,
                                instance.amount, instance.label))
    else:
        accounting_log.info("Updating payment %s (Date: %s, Member: %s, Amount: %s, Label: %s, Allocated: %s)."
                            % (instance.pk, instance.date, instance.member,
                                instance.amount, instance.label,
                                instance.amount_already_allocated()))

    # If this payment is related to a member, update the accounting for
    # this member
    if (created or instance.amount_not_allocated() != 0) \
    and (instance.member is not None):
        update_accounting_for_member(instance.member)


@receiver(post_save, sender=Invoice)
@disable_for_loaddata
def invoice_changed(sender, instance, created, **kwargs):

    if created:
        accounting_log.info(
            "Creating draft invoice DRAFT-{} (Member: {}).".format(
                instance.pk, instance.member))
    else:
        if not instance.validated:
            accounting_log.info(
                "Updating draft invoice DRAFT-{} (Member: {}).".format(
                    instance.number, instance.member))
        else:
            accounting_log.info(
                "Updating invoice {} (Member: {}, Total amount: {}, Amount paid: {}).".format(
                    instance.number, instance.member,
                    instance.amount(), instance.amount_paid()))

@receiver(post_delete, sender=PaymentAllocation)
def paymentallocation_deleted(sender, instance, **kwargs):

    invoice = instance.invoice

    # Reopen invoice if relevant
    if (invoice.amount_remaining_to_pay() > 0) and (invoice.status == "closed"):
        accounting_log.info("Reopening invoice {} ...".format(invoice.number))
        invoice.status = "open"
        invoice.save()


@receiver(post_delete, sender=Payment)
def payment_deleted(sender, instance, **kwargs):

    accounting_log.info(
        "Deleted payment {} (Date: {}, Member: {}, Amount: {}, Label: {}).".forma(
            instance.pk, instance.date, instance.member, instance.amount, instance.label))

    member = instance.member

    if member is None:
        return

    this_member_invoices = [i for i in member.invoices.filter(validated=True).order_by("date")]
    this_member_payments = [p for p in member.payments.order_by("date")]

    member.balance = compute_balance(this_member_invoices,
                                     this_member_payments)
    member.save()


