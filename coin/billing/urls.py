# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, url
from django.views.generic import DetailView
from coin.billing import views

urlpatterns = patterns(
    '',
    url(r'^invoice/(?P<id>.+)/pdf$', views.invoice_pdf, name="invoice_pdf"),
    url(r'^invoice/(?P<id>.+)$', views.invoice, name="invoice"),
    # url(r'^invoice/(?P<id>.+)/validate$', views.invoice_validate, name="invoice_validate"),

    url('invoice/create_all_members_invoices_for_a_period', views.gen_invoices)
)
