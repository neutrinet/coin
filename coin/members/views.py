# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template import RequestContext
from django.shortcuts import render_to_response, render
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.conf import settings
from forms import PersonMemberChangeForm, OrganizationMemberChangeForm

@login_required
def index(request):
    has_isp_feed = 'isp' in [k for k, _, _ in settings.FEEDS]
    return render_to_response('members/index.html',
                              {'has_isp_feed': has_isp_feed},
                              context_instance=RequestContext(request))


@login_required
def detail(request):

    membership_info_url = settings.MEMBER_MEMBERSHIP_INFO_URL
    context={
        'membership_info_url': membership_info_url,
    }

    if settings.MEMBER_CAN_EDIT_PROFILE:
        if request.user.type == "natural_person":
            form_cls = PersonMemberChangeForm
        else:
            form_cls = OrganizationMemberChangeForm

        if request.method == "POST":
            form = form_cls(data = request.POST, instance = request.user)
            if form.is_valid():
                form.save()
        else:
            form = form_cls(instance = request.user)

        context['form'] = form

    return render(request, 'members/detail.html', context)


@login_required
def subscriptions(request):
    subscriptions = request.user.get_active_subscriptions()
    old_subscriptions = request.user.get_inactive_subscriptions()

    return render_to_response('members/subscriptions.html',
                              {'subscriptions': subscriptions,
                               'old_subscriptions': old_subscriptions},
                              context_instance=RequestContext(request))


@login_required
def invoices(request):
    balance  = request.user.balance
    invoices = request.user.invoices.filter(validated=True).order_by('-date')
    payments = request.user.payments.filter().order_by('-date')

    return render_to_response('members/invoices.html',
                              {'balance' : balance, 
                               'invoices': invoices, 
                               'payments': payments},
                              context_instance=RequestContext(request))


@login_required
def contact(request):
    return render_to_response('members/contact.html',
                              context_instance=RequestContext(request))
