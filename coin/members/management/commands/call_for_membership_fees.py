# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from dateutil.relativedelta import relativedelta
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Max
from django.conf import settings

from coin.utils import respect_language
from coin.members.models import Member, MembershipFee


class Command(BaseCommand):
    args = '[date=2011-07-04]'
    help = """Send a call for membership email to members.
              A mail is sent when end date of membership
              reach the anniversary date, 1 month before and once a month
              for 3 months.
              By default, today is used to compute relative dates, but a date
              can be passed as argument."""

    def handle(self, *args, **options):
        verbosity = int(options['verbosity'])
        try:
            date = datetime.datetime.strptime(args[0], '%Y-%m-%d').date()
        except IndexError:
            date = datetime.date.today()
        except ValueError:
            raise CommandError(
                'Please enter a valid date : YYYY-mm-dd (ex: 2011-07-04)')

        end_dates = [date + relativedelta(months=-3),
                     date + relativedelta(months=-2),
                     date + relativedelta(months=-1),
                     date,
                     date + relativedelta(months=+1)]

        if verbosity >= 2:
            self.stdout.write("Selecting members whose membership fee end at "
                              "the following dates : {dates}".format(
                                  dates=[str(d) for d in end_dates]))

        members = Member.objects.filter(status='member')\
                                .annotate(end=Max('membership_fees__end_date'))\
                                .filter(end__in=end_dates)\
                                .filter(send_membership_fees_email=True)
        if verbosity >= 2:
            self.stdout.write(
                "Got {number} members.".format(number=members.count()))

        cpt = 0
        with respect_language(settings.LANGUAGE_CODE):
            for member in members:
                if member.send_call_for_membership_fees_email(auto=True):
                    self.stdout.write(
                        'Call for membership fees email was sent to {member} ({email})'.format(
                            member=member, email=member.email))
                    cpt = cpt + 1

        if cpt > 0 or verbosity >= 2:
            self.stdout.write("{number} call for membership fees emails were "
                              "sent".format(number=cpt))
