# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.admin import SimpleListFilter
import datetime


class MembershipFeeFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Cotisations'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'fee'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('paidup', 'À jour de cotisation'),
            ('late', 'En retard'),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value() == 'paidup':
            return queryset.filter(
                membership_fees__start_date__lte=datetime.date.today,
                membership_fees__end_date__gte=datetime.date.today)
        if self.value() == 'late':
            return queryset.filter(status='member').exclude(
                membership_fees__start_date__lte=datetime.date.today,
                membership_fees__end_date__gte=datetime.date.today)
