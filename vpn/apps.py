# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
import coin.apps

from . import urls


class VPNConfig(AppConfig, coin.apps.AppURLs):
    name = 'vpn'
    verbose_name = "Gestion d'accès VPN"

    exported_urlpatterns = [('vpn', urls.urlpatterns)]
